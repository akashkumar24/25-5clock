const { useState, useEffect } = React;

const Defaults = [5, 25, 25 * 60, false, false];

function App() {
  const [breakLength, setBreakLength] = useState(Defaults[0]);
  const [sessionLength, setSessionLength] = useState(Defaults[1]);
  const [currentTimeLeft, setCurrentTimeLeft] = useState(Defaults[2]);
  const [isBreakTime, setIsBreakTime] = useState(Defaults[3]);
  const [isRunning, setIsRunning] = useState(Defaults[4]);
  const [clockID, setClockID] = useState(null);

  useEffect(() => {
    return () => {
      if (clockID !== null) {
        clearInterval(clockID);
      }
    };
  }, [clockID]);

  useEffect(() => {
    setCurrentTimeLeft(isBreakTime ? breakLength * 60 : sessionLength * 60);
  }, [breakLength, sessionLength, isBreakTime]);

  const changeTimerLength = (type, amount) => {
    if (!isRunning) {
      if (type === "Break") {
        setBreakLength((prevBreakLength) =>
          Math.min(60, Math.max(1, prevBreakLength + amount))
        );
      } else {
        setSessionLength((prevSessionLength) =>
          Math.min(60, Math.max(1, prevSessionLength + amount))
        );
      }
    }
  };

  const tick = () => {
    setCurrentTimeLeft((prevTimeLeft) => {
      if (prevTimeLeft === 0) {
        ring();
        const nextIsBreakTime = !isBreakTime;
        setIsBreakTime(nextIsBreakTime);
        return nextIsBreakTime ? breakLength * 60 : sessionLength * 60;
      }
      return prevTimeLeft - 1;
    });
  };

  const ring = () => {
    const audio = document.getElementById("beep");
    audio.play();
  };

  const toggleTimer = () => {
    if (clockID !== null) {
      clearInterval(clockID);
      setClockID(null);
    } else {
      const newClockID = setInterval(tick, 1000);
      setClockID(newClockID);
    }
    setIsRunning((prevIsRunning) => !prevIsRunning);
  };

  const resetTimer = () => {
    const audio = document.getElementById("beep");
    audio.pause();
    audio.currentTime = 0;

    if (clockID !== null) {
      clearInterval(clockID);
      setClockID(null);
    }
    setBreakLength(Defaults[0]);
    setSessionLength(Defaults[1]);
    setCurrentTimeLeft(Defaults[2]);
    setIsBreakTime(Defaults[3]);
    setIsRunning(Defaults[4]);
  };

  return (
    <div id="clock">
      <div id="clockBody">
        <h2 className="topheader">25+5 Clock</h2>
        <section id="timerLengthSection">
          <TimerLengthDisplay
            name="Break"
            length={breakLength}
            buttonClick={changeTimerLength}
          />

          <TimerLengthDisplay
            name="Session"
            length={sessionLength}
            buttonClick={changeTimerLength}
          />
        </section>
        <section id="timerSection">
          <TimerDisplay time={currentTimeLeft} isBreak={isBreakTime} />
          <TimerControls
            play={toggleTimer}
            reset={resetTimer}
            isPaused={isRunning}
          />
        </section>
      </div>
      <audio
        id="beep"
        preload="auto"
        src="https://raw.githubusercontent.com/freeCodeCamp/cdn/master/build/testable-projects-fcc/audio/BeepSound.wav"
      ></audio>
    </div>
  );
}

function TimerLengthDisplay({ name, length, buttonClick }) {
  return (
    <div className="clockLength">
      <label className="cremendterLabel" id={name.toLowerCase() + "-label"}>
        {name + " Length"}
      </label>
      <div className="cremendters">
        <Incrementer name={name} buttonClick={buttonClick} />
        <p className="numberDisplay" id={name.toLowerCase() + "-length"}>
          {length}
        </p>
        <Decrementer name={name} buttonClick={buttonClick} />
      </div>
    </div>
  );
}

function Decrementer({ name, buttonClick }) {
  return (
    <div>
      <button
        className="decrementer"
        id={name.toLowerCase() + "-decrement"}
        onClick={() => buttonClick(name, -1)}
      >
        <i className="bi bi-arrow-down"></i>
      </button>
    </div>
  );
}

function Incrementer({ name, buttonClick }) {
  return (
    <div>
      <button
        className="incrementer"
        id={name.toLowerCase() + "-increment"}
        onClick={() => buttonClick(name, 1)}
      >
        <i className="bi bi-arrow-up"></i>
      </button>
    </div>
  );
}

function TimerDisplay({ time, isBreak }) {
  const formatTime = (time) => {
    let minutes = Math.floor(time / 60);
    let seconds = time % 60;

    if (minutes < 10) {
      minutes = "0" + minutes;
    }
    if (seconds < 10) {
      seconds = "0" + seconds;
    }
    return minutes + ":" + seconds;
  };

  const timerLabel = isBreak ? "Break" : "Session";

  return (
    <div id="timer">
      <h3 id="timer-label">{timerLabel}</h3>
      <h2 id="time-left">{formatTime(time)}</h2>
    </div>
  );
}

function TimerControls({ play, reset, isPaused }) {
  return (
    <div id="clock-controls">
      <button id="start_stop" onClick={play}>
        {isPaused ? (
          <i className="bi bi-pause-circle-fill"></i>
        ) : (
          <i className="bi bi-play-circle-fill"></i>
        )}
      </button>
      <button id="reset" onClick={reset}>
        <i className="bi bi-arrow-counterclockwise"></i>
      </button>
    </div>
  );
}

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
